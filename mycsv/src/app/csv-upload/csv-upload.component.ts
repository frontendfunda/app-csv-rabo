import { Component, OnInit } from '@angular/core';
import { ReadCsvService } from '../read-csv.service';


@Component({
  selector: 'app-csv-upload',
  templateUrl: './csv-upload.component.html',
  styleUrls: ['./csv-upload.component.scss']
})
export class CsvUploadComponent implements OnInit {

  // File upload Inputs
  files: any = [];
  fileDetail: any;


  constructor(private services: ReadCsvService) { }

  ngOnInit(): void {
  }

  // File upload Method
  uploadFile(input: HTMLInputElement) {
    for (let i = 0; i < input.files.length; i++) {
      const element = input.files[i];
      this.files.push(element);
    }
    const files = input.files;
    this.fileDetail = files[0];
    if (files && files.length) {
      const fileToRead = files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        this.services.csvData.emit(e.target.result);
      };
      fileReader.readAsText(fileToRead, 'UTF-8');
    }
  }
}
