import { Component, OnInit, ViewChild } from '@angular/core';
import { ReadCsvService } from '../read-csv.service';
import { MatTableDataSource } from '@angular/material/table';
import { Sort } from '@angular/material/sort';
import { PageEvent } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';

export interface CsvDataList {
  Firstname: string;
  Surname: number;
  Issuecount: number;
  Dateofbirth: string;
}

@Component({
  selector: 'app-csv-list',
  templateUrl: './csv-list.component.html',
  styleUrls: ['./csv-list.component.scss']
})
export class CsvListComponent implements OnInit {

  // Filter Inputs
  toppings = new FormControl();
  toppingList: string[];

  // MatTabl Inputs
  displayedColumns: string[] = ['Firstname', 'Surname', 'Issuecount', 'Dateofbirth'];
  dataSource: MatTableDataSource<CsvDataList>;

  csvData: any[];
  sortedData: CsvDataList[];
  filterList: any[];

  // MatPaginator Inputs
  length = 0;
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  constructor(private services: ReadCsvService) {
  }

// MatPaginator Method
  onPageChange(e: any) {
    this.pageIndex = e.pageIndex;
    this.pageSize = e.pageSize;
    this.loadData(this.pageIndex, this.pageSize);
  }
// MatPaginator Method
  loadData(pageIndex: number, pageSize: number) {
    if (this.filterList === undefined || this.filterList === null || this.filterList.length === 0) {
      this.length = this.csvData.length;
      this.dataSource = new MatTableDataSource(this.csvData.slice(pageIndex, pageIndex + pageSize));
      return;
    }
    this.length = this.filterList.length;
    const filterData = this.csvData.slice().filter(csvData => this.filterList.includes(csvData.Issuecount));
    this.dataSource = new MatTableDataSource(filterData.slice(pageIndex, pageIndex + pageSize));
  }

  ngOnInit(): void {

    // Csv Data Fetch via Services
    this.services.csvData.subscribe(
      (csvResult: string) => {
        const textFromFileLoaded = csvResult;
        const txt = textFromFileLoaded;
        const csv = [];
        const lines = txt.split(/\r\n|\n/);
        lines.forEach((element: any) => {
          const cols: string[] = element.split(';');
          csv.push(cols);
        });
        this.csvData = this.services.csvParser(csv);
        this.length = this.csvData.length;

        // Set Unique value for Filter option
        const fiterList = [];
        this.csvData.forEach((csvData) => { fiterList.push(csvData.Issuecount); });
        this.toppingList = [... new Set(fiterList)];

        this.loadData(this.pageIndex, this.pageSize);
      }
    );

  }

  // Issue Count Filter method
  getFilterData(e: any) {
    if (!e) {
      this.filterList = this.toppings.value;
      if (this.filterList === undefined || this.filterList === null || this.filterList.length === 0) {
        this.length = this.csvData.length;
        this.dataSource = new MatTableDataSource(this.csvData.slice(this.pageIndex, this.pageIndex + this.pageSize));
        return;
      }
      this.length = this.filterList.length;
      const filterData = this.csvData.slice().filter(csvData => this.filterList.includes(csvData.Issuecount));
      this.dataSource = new MatTableDataSource(filterData.slice(this.pageIndex, this.pageIndex + this.pageSize));
    }
  }

  // Sorting option for all the colomn
  sortData(sort: Sort) {
    const data = this.csvData.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = new MatTableDataSource(data.slice(this.pageIndex, this.pageIndex + this.pageSize));
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Firstname': return compare(a.Firstname, b.Firstname, isAsc);
        case 'Surname': return compare(a.Surname, b.Surname, isAsc);
        case 'Issuecount': return compare(a.Issuecount, b.Issuecount, isAsc);
        case 'Dateofbirth': return compare(a.Dateofbirth, b.Dateofbirth, isAsc);
        default: return 0;
      }
    });
    this.dataSource = new MatTableDataSource(this.sortedData.slice(this.pageIndex, this.pageIndex + this.pageSize));
  }
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
