import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReadCsvService {

  // Communication between Csv upload componant to Csv List Componant
  csvData = new EventEmitter<string | ArrayBuffer>();

  // Json generator Input
  title: string[];
  csvArray: any[] = [];

  constructor() { }

  csvParser(value: string[]) {
    value.forEach((element, index) => {
      if (index === 0) {
        // Title array
        this.title = element.toString().replace(/"/g, '').replace(/\s/g, '').split(';');
      } else {
        // To set Key and Value : Json generator
        const y = element.toString().replace(/"/g, '').split(',');
        const result: object = {};
        this.title.forEach((key, i) => key === 'Dateofbirth' ? result[key] = this.dateFormat(y[i]) : result[key] = y[i]);
        this.csvArray.splice(index - 1, 0, result);
      }
    });
    return this.csvArray;
  }

  dateFormat(dob: string) {
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const dateObj = new Date(dob);
    const month = monthNames[dateObj.getMonth()];
    const day = String(dateObj.getDate()).padStart(2, '0');
    const year = dateObj.getFullYear();
    const output = day + ', ' + month + ', ' + year;
    return output;
  }

}
